
README file for the Location Facets Drupal module.


Description
***********

Location Facets exposes geographical information associated with Drupal nodes
as facets for the Faceted Search module. For its geographical information
Location Facets relies on the Location module.

Location Facets currently only exposes regular locations associated with nodes,
it does not support CCK Location fields.

Requirements
************

- Drupal 6.x (http://drupal.org/project/drupal).

- Faceted Search (http://drupal.org/project/faceted_search).

- Location (http://drupal.org/project/location).


Installation
************

1. Extract the location_facets package into your Drupal modules directory.

2. Go to the Administer > Site building > Modules page, and enable the Location
   Facets module.

3. Go to the Administer > Site configuration > Faceted Search page, edit the
   faceted search environment that shall expose the Location-based facet. In the
   environment editing form, check the location facet.

Support
*******

For support requests, bug reports, and feature requests, please use the
project's issue queue on http://drupal.org/project/issues/location_facets.

Please DO NOT send bug reports through e-mail or personal contact forms, use the
aforementioned issue queue instead.


Credits
*******

- Developed by Kristof Coomans at Statik (http://www.statik.be)
