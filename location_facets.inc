<?php

module_load_include('inc', 'faceted_search');

class location_facet extends faceted_search_facet {

  /**
   * Constructor.
   *
   * @param $active_path
   *   Array representing the path leading to the active category, including the
   *   active category itself. Defaults to an empty array, meaning no active
   *   category.
   */
  function __construct($active_path = array()) {
    parent::faceted_search_facet('location', $active_path);
  }

  /**
   * Return the id of this facet.
   */
  function get_id() {
    return 1; // This module provides only one facet.
  }

  /**
   * Return the label of this facet. The implementor is responsible to ensure
   * adequate security filtering.
   */
  function get_label() {
    return t('Location');
  }

  /**
   * Return the search text for this facet, taking into account this facet's
   * active path.
   */
  function get_text() {
    if ($category = $this->get_active_category()) {
      $location = $category->country;
      if (isset($category->city)) {
        $location .= ', ' . $category->city;
      }

      return '"' . faceted_search_quoted_query_escape($location) . '"';
    }
    return '';
  }

  /**
   * Return the field name to help site administrators identify fields.
   */
  function get_help() {
    return 'location';
  }

  /**
   * Returns the available sort options for this facet.
   */
  function get_sort_options() {
    $options = parent::get_sort_options();
    $options['location'] = t('Location');
    return $options;
  }

  /**
   * Handler for the 'count' sort criteria.
   */
  function build_sort_query_count(&$query) {
    $query->add_orderby('count', 'DESC');
    $this->build_sort_query_location($query);
  }

  /**
   * Handler for the 'location' sort criteria.
   */
  function build_sort_query_location(&$query) {
  if ($query->has_field('location_country')) {
      $query->add_orderby('location_country', 'ASC');
    }
    if ($query->has_field('location_city')) {
      $query->add_orderby('location_city', 'ASC');
    }
  }

  /**
   * Updates a query for retrieving the root categories of this facet and their
   * associated nodes within the current search results.
   *
   * @param $query
   *   The query object to update.
   * @return
   *   FALSE if this facet can't have root categories.
   */
  function build_root_categories_query(&$query) {
    $query->add_table('location_instance', 'vid', 'n', 'vid');
    $query->add_table('location', 'lid', 'location_instance', 'lid');
    $query->add_field('location', 'country');
    $query->add_groupby('location_country');
    return TRUE;
  }

  /**
   * This factory method creates categories given query results that include the
   * fields selected in get_root_categories_query() or get_subcategories_query().
   *
   * @param $results
   *   $results A database query result resource.
   * @return
   *   Array of categories.
   */
  function build_categories($results) {
    $categories = array();
    while ($result = db_fetch_object($results)) {
      //var_dump($result);
      $country = $result->location_country;
      $city = isset($result->location_city) ? $result->location_city: NULL;
      $categories[] = new location_facet_category($country, $city, $result->count);
    }

    return $categories;
  }
}

class location_facet_category extends faceted_search_category {

  public $country;
  public $city;

  function __construct($country, $city = NULL, $count = NULL) {
    parent::faceted_search_category($count);
    $this->country = $country;
    $this->city = $city;
  }

  /**
   * Return the label of this category.
   *
   * @param $html
   *   TRUE when HTML is allowed in the label, FALSE otherwise. Checking this
   *   flag allows implementors to provide a rich-text label if desired, and an
   *   alternate plain text version for cases where HTML cannot be used. The
   *   implementor is responsible to ensure adequate security filtering.
   */
  function get_label($html = FALSE) {
    if (isset($this->city)) {
      return check_plain($this->city);
    }
    else {
      return check_plain(location_country_name($this->country));
    }
  }

  /**
   * Updates a query for selecting nodes matching this category.
   *
   * @param $query
   *   The query object to update.
   */
  function build_results_query(&$query) {
    $query->add_table('location_instance', 'vid', 'n', 'vid');
    $query->add_table('location', 'lid', 'location_instance', 'lid');
    $query->add_where("location.country = '%s'", $this->country);

    if (isset($this->city)) {
      $query->add_where("location.city = '%s'", $this->city);
    }
  }

  function build_subcategories_query(&$query) {
    if (isset($this->city)) {
      return FALSE;
    }

    $query->add_table('location_instance', 'vid', 'n', 'vid');
    $query->add_table('location', 'lid', 'location_instance', 'lid');
    $query->add_where("location.country='%s'", $this->country);
    $query->add_field('location', 'country');

    $alias = $query->add_field('location', 'city');
    $query->add_groupby($alias);
    return TRUE;
  }
}